package com.platzi.javatests.Util;

public class StringUtil {

    public static String repeat(String str, int times){

        if(times < 0){
            throw new IllegalArgumentException("Negative times cant be negative");
        }

        String resultado = "";

        for (int i = 0; i < times; i++) {
            resultado += str;
        }
        return resultado;
    }

    public static boolean isEmpty(String str) {
            return str == null || str.trim().length() <= 0;
    }
}
