package com.platzi.javatests.Util;


import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertTrue;

public class StringUtilTest {

    @Test
    public void testRepeat() {
        Assert.assertEquals("HolaHolaHola", StringUtil.repeat("Hola", 3));
    }

    @Test
    public void testRepeatStringOnce() {
        Assert.assertEquals("Hola", StringUtil.repeat("Hola", 1));
    }

    @Test
    public void testRepeatStringZeroTimes() {
        Assert.assertEquals("", StringUtil.repeat("Hola", 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRepeatStringNegativeTimes() {
        StringUtil.repeat("Hola", -1);
    }

    @Test
    public void string_is_not_empty() {
        assertTrue(StringUtil.isEmpty(Mockito.anyString()));
    }

    @Test
    public void string_is_empty() {
        assertTrue(StringUtil.isEmpty(""));
    }

    @Test
    public void string_is_null() {
        assertTrue(StringUtil.isEmpty(Mockito.isNull()));
    }
}