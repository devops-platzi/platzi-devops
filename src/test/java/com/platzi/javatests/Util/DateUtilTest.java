package com.platzi.javatests.Util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DateUtilTest {

    @Test
    public void return_true_when_year_is_divisible_by_400() {

        assertThat(DateUtil.isLeakYear(1600), is(true));
        assertThat(DateUtil.isLeakYear(2400), is(true));
        assertThat(DateUtil.isLeakYear(2000), is(true));
    }
}