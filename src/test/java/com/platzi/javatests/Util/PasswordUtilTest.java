package com.platzi.javatests.Util;

import org.junit.Test;

import static com.platzi.javatests.Util.PasswordUtil.SecurityLevel.*;
import static org.junit.Assert.*;

public class PasswordUtilTest {

    @Test
    public void weak_when_has_less_than_eigth_letters() {
        assertEquals(WEAK,PasswordUtil.asssestPassword("1234567"));
    }

    @Test
    public void weak_when_has_only_letters() {
        assertEquals(WEAK,PasswordUtil.asssestPassword("ab12!"));
    }

    @Test
    public void medium_when_has_letters_and_numbers() {
        assertEquals(MEDIUM,PasswordUtil.asssestPassword("abcdefgh1234"));
    }

    @Test
    public void strong_when_has_letters_numbers_and_symbols() {
        assertEquals(STRONG,PasswordUtil.asssestPassword("abcd1234*"));
    }
}